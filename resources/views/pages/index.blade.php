@extends('layouts.master')

@section('content')

    <script src="/homer/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/homer/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">

                <div id="hbreadcrumb" class="pull-right m-t-lg">
                    <ol class="hbreadcrumb breadcrumb">
                        <li>
                            <a href="{{ route('vacancy.create') }}">
                                <button class="btn btn-info"><i class="fa fa-plus"></i> Create</button>
                            </a>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Vacancies
                </h2>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="article_table" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Created At</th>
                                @auth
                                    @if(Auth::user()->is_admin === 1)
                                        <th>actions</th>
                                    @endif
                                @endauth
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vacancies as $vacancy)
                                <tr>
                                    <td>{{ $vacancy->title }}</td>
                                    <td>{{ $vacancy->created_at }}</td>
                                    @auth
                                        @if(Auth::user()->is_admin === 1)
                                            <td>
                                                <a href="{{ route('vacancy.edit', ['id' => $vacancy->id]) }}"><span><i class="fa fa-edit"></i></span></a>
                                                -
                                                <a onclick="event.preventDefault();
                                                    document.getElementById('vacancy-{{ $vacancy->id }}').submit();">
                                                    <span><i class="fa fa-trash"></i></span>
                                                </a>
                                                <form id="vacancy-{{ $vacancy->id }}" action="{{ route('vacancy.destroy', ['id' => $vacancy->id]) }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                </form>
                                            </td>
                                        @endif
                                    @endauth
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // Initialize Example 2
        $('#article_table').dataTable();

    </script>
@endsection
