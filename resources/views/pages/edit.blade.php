@extends('layouts.master')

@section('content')
    <link rel="stylesheet" href="/homer/vendor/summernote/dist/summernote.css" />
    <link rel="stylesheet" href="/homer/vendor/summernote/dist/summernote-bs3.css" />
    <link rel="stylesheet" href="/homer/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="/homer/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />


    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        Update Vacancy
                    </div>
                    <div class="panel-body">

                        @foreach ($errors->all() as $message)
                            <div class="alert-danger">{{ $message }}</div>
                        @endforeach

                        <form method="post" action="{{ route('vacancy.update', ['id' => $vacancy->id]) }}" class="form-horizontal">

                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title" id="article_title" value="{{ old('title', $vacancy->title) }}" class="form-control" required>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Content</label>
                                <div class="col-sm-10">
                                    <textarea class="input-block-level summernote" name="content" rows="18" required>{!! old('content', $vacancy->content) !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/homer/vendor/summernote/dist/summernote.min.js"></script>

    <script>

        $(function () {
            $('.summernote').summernote({ height: 300});
        });
    </script>
@endsection
