<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Homer template</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="/homer/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="/homer/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="/homer/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="/homer/vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="/homer/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="/homer/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="/homer/styles/style.css">

    <script src="/homer/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/homer/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/homer/vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="/homer/vendor/bootstrap/dist/js/bootstrap.min.js"></script>

</head>
