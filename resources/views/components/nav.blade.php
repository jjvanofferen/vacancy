<aside id="menu">
    <div id="navigation">
       <ul class="nav" id="side-menu">
            <li class="active">
                <a href="{{ route('vacancy.index') }}"> <span class="nav-label">Vacatures</span></a>
            </li>

           @auth
               <li class="active">
                   <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                       {{ __('Logout') }}
                   </a>

                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                       @csrf
                   </form>
               </li>

           @else
               <li class="active">
                   <a href="{{ route('login') }}"> <span class="nav-label">Login</span></a>
               </li>
           @endauth

        </ul>
    </div>
</aside>
