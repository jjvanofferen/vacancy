@include('components.header')


<body class="fixed-navbar sidebar-scroll">

@include('components.top-nav')

<!-- Navigation -->
@include('components.nav')

<!-- Main Wrapper -->
<div id="wrapper">

    @yield('content')

    <!-- Right sidebar -->
    @include('components.newsletter')
    <!-- Footer-->
    <footer class="footer">
        <span class="pull-right">
           Powered by: Homer template
        </span>
        Jessey van Offeren
    </footer>

</div>

<!-- Vendor scripts -->
<script src="/homer/vendor/jquery-flot/jquery.flot.js"></script>
<script src="/homer/vendor/jquery-flot/jquery.flot.resize.js"></script>
<script src="/homer/vendor/jquery-flot/jquery.flot.pie.js"></script>
<script src="/homer/vendor/flot.curvedlines/curvedLines.js"></script>
<script src="/homer/vendor/jquery.flot.spline/index.js"></script>
<script src="/homer/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="/homer/vendor/iCheck/icheck.min.js"></script>
<script src="/homer/vendor/peity/jquery.peity.min.js"></script>
<script src="/homer/vendor/sparkline/index.js"></script>

<!-- App scripts -->
<script src="/homer/scripts/homer.js"></script>

</body>
</html>
