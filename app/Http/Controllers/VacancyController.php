<?php

namespace App\Http\Controllers;

use App\Http\Requests\VacancyStoreRequest;
use App\Http\Requests\VacancyUpdateRequest;
use App\Services\VacancyFilterService;
use App\Services\VacancyService;
use App\Vacancy;

class VacancyController extends Controller
{
    /**
     * @var VacancyFilterService
     */
    private $vacancyFilterService;
    /**
     * @var VacancyService
     */
    private $vacancyService;

    /**
     * VacancyController constructor.
     * @param VacancyFilterService $vacancyFilterService
     * @param VacancyService $vacancyService
     */
    public function __construct(VacancyFilterService $vacancyFilterService, VacancyService $vacancyService)
    {
        $this->middleware('is_admin')->only(['edit', 'update', 'destroy']);

        $this->vacancyFilterService = $vacancyFilterService;
        $this->vacancyService = $vacancyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::get();

        return view('pages.index', compact('vacancies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VacancyStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VacancyStoreRequest $request)
    {
        $vacancy = $this->vacancyService->store($request);

        if (!$vacancy) {
            return back()->withInput($request->all());
        }

        return redirect(route('vacancy.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacancy $vacancy)
    {
        return view('pages.edit', compact('vacancy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param VacancyUpdateRequest $request
     * @param Vacancy $vacancy
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(VacancyUpdateRequest $request, Vacancy $vacancy)
    {
        $this->vacancyService->update($request, $vacancy);

        return redirect(route('vacancy.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vacancy $vacancy
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Vacancy $vacancy)
    {
        $vacancy->delete();

        return redirect(route('vacancy.index'));
    }
}
