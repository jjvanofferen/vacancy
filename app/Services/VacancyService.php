<?php

namespace App\Services;

use App\Http\Requests\VacancyStoreRequest;
use App\Http\Requests\VacancyUpdateRequest;
use App\Vacancy;

class VacancyService {

    /**
     * @var VacancyFilterService
     */
    private $vacancyFilterService;

    /**
     * VacancyService constructor.
     * @param VacancyFilterService $vacancyFilterService
     */
    public function __construct(VacancyFilterService $vacancyFilterService)
    {
        $this->vacancyFilterService = $vacancyFilterService;
    }

    /**
     * @param VacancyStoreRequest $request
     * @return Vacancy
     */
    public function store(VacancyStoreRequest $request)
    {
        /** @var $content String **/
        $content = $this->vacancyFilterService->stripString($request->input('content'));

        /** @var Vacancy $vacancy */
        $vacancy = Vacancy::create([
            'title' => $request->input('title'),
            'content' => $content
        ]);

        return $vacancy;
    }

    public function update(VacancyUpdateRequest $request, Vacancy $vacancy)
    {
        /** @var $content String */
        $content = $this->vacancyFilterService->stripString($request->input('content'));

        $vacancy->update([
            'title' => $request->input('title'),
            'content' => $content
        ]);

        return $vacancy;
    }
}
