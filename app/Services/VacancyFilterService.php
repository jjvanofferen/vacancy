<?php

namespace App\Services;

class VacancyFilterService {

    /**
     *
     * @param String $input
     *
     * @return String $string
     */
	public function stripString(String $input)
	{
        return preg_replace(['#<script(.*?)>(.*?)</script>#is', '#&lt;script(.*?)&gt;(.*?)&lt;/script&gt;#is'], '', $input);
	}
}
