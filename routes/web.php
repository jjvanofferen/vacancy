<?php

Auth::routes();

Route::get('/', 'VacancyController@index');

Route::resource('vacancy', 'VacancyController')->except([
    'show'
]);
