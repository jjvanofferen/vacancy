<?php

use Illuminate\Database\Seeder;

class createAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => "Admin",
            'email' => 'admin@example.com',
            'password' => \Illuminate\Support\Facades\Hash::make('administrator'),
            'is_admin' => 1
        ]);
    }
}
