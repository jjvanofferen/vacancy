<?php

namespace Tests\Feature;

use App\Vacancy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VacancyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Testing to store vacancy as guest
     * Resulting in a redirect to the home page
     * Stored variable has the title set.
     *
     * @return void
     */
    public function testStoringVacancy()
    {
        $response = $this->post(route('vacancy.store'), ['title' => 'My fake title', 'content' => 'my fake content']);

        $response->assertRedirect(route('vacancy.index'));

        $vacancy = Vacancy::get()->last();

        $response->assertRedirect(route('vacancy.index'));

        $this->assertTrue($vacancy->title === 'My fake title');
    }
}
