<?php

namespace Tests\Feature;

use App\Vacancy;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class HttpTest
 * @package Tests\Feature
 *
 * This class will only test all possible HTTP responses from the application.
 * This is to validate that pages are loading and redirecting correctly when required.
 */
class HttpTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Testing home page returning 200 status
     * Resulting http status 200, page should be working for everyone.
     *
     * @return void
     */
    public function testHomePageShowing()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Testing vacancy page returning 200 status
     * Resulting http status 200, page should be working for everyone.
     *
     * @return void
     */
    public function testVacancyPageShowing()
    {
        $response = $this->get('/vacancy');

        $response->assertStatus(200);
    }

    /**
     * Testing vacancy create page returning 200 status
     * Resulting http status 200, page should be working for everyone.
     *
     * @return void
     */
    public function testVacancyCreatePageShowing()
    {
        $response = $this->get('/vacancy/create');

        $response->assertStatus(200);
    }

    /**
     * Testing vacancy edit page as guest
     * Resulting http status 403, forbidden access.
     *
     * Current user (guest) is not an admin.
     *
     * @return void
     */
    public function testVacancyEditPageAsGuest()
    {
        $vacancy = factory(Vacancy::class)->create();

        $this->get(route('vacancy.edit', ['id' => $vacancy->id]))
            ->assertStatus(403);
    }

    /**
     * Testing login page route
     * Resulting http status 200, login page should work for everyone.
     *
     * @return void
     */
    public function testLoginPageShowing()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }
}
