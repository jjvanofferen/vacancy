<?php

namespace Tests\Feature;

use App\User;
use App\Vacancy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTests extends TestCase
{
    use RefreshDatabase;

    /**
     * Testing if an admin can visit the edit page.
     * Resulting in a http status of 200, admin should be able to access the route
     *
     * @return void
     */
    public function testAdminVisitingEditPage()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@example.com',
            'is_admin' => 1
        ]);

        $vacancy = factory(Vacancy::class)->create();

        $this->actingAs($user)
            ->get(route('vacancy.edit', ['id' => $vacancy->id]))
            ->assertStatus(200)
            ->assertSee($vacancy->title);
    }

    /**
     * Testing if guests can visit the edit page which should be protected.
     * Resulting in a http status 403, forbidden access.
     *
     * @return void
     */
    public function testVisitingAdminRouteWithoutPermission()
    {
        $vacancy = factory(Vacancy::class)->create();

        $this->get(route('vacancy.edit', ['id' => $vacancy->id]))
            ->assertStatus(403);
    }

    /**
     * Testing if a guest is able to update a vacancy
     * Resulting in a http status 403, forbidden access.
     *
     * @return void
     */
    public function testGuestEditingVacancy()
    {
        $vacancy = factory(Vacancy::class)->create();

        $this->put(route('vacancy.update', ['id' => $vacancy->id]), ['title' => $vacancy->title, 'content' => "I changed the content"])
            ->assertStatus(403);
    }

    /**
     * Testing if an admin can delete a vacancy.
     * Resulting in redirect and not seeing the title of a vacancy anymore.
     *
     * @return void
     */
    public function testAdminDeletingVacancy()
    {
        $vacancy = factory(Vacancy::class)->create();

        $user = factory(User::class)->create([
            'email' => 'admin@example.com',
            'is_admin' => 1
        ]);

        $this->actingAs($user)
            ->delete(route('vacancy.destroy', ['id' => $vacancy->id]))
            ->assertRedirect(route('vacancy.index'))
            ->assertDontSee($vacancy->title);
    }

    /**
     * Testing if a guest is able to delete a vacancy without permissions.
     * Resulting in http status 403, forbidden access.
     *
     * @return void
     */
    public function testGuestTestingToDeleteVacancy()
    {
        $vacancy = factory(Vacancy::class)->create();

        $this->delete(route('vacancy.destroy', ['id' => $vacancy->id]))
            ->assertStatus(403);
    }
}
